$(document).ready(function(){
        var fileInput = $('.file-input');
        var droparea = $('.file-drop-area');

        // highlight drag area
        fileInput.on('dragenter focus click', function() {
        droparea.addClass('is-active');
        });

        // back to normal state
        fileInput.on('dragleave blur drop', function() {
        droparea.removeClass('is-active');
        });

        // change inner text
        fileInput.on('change', function() {
        var filesCount = $(this)[0].files.length;
        var textContainer = $(this).prev('.js-set-number');

        if (filesCount === 1) {
            // if single file then show file name
            textContainer.text($(this).val().split('\\').pop());
        } else {
            // otherwise show number of files
            textContainer.text(filesCount + ' files selected');
        }
            });
      $('#icon-new').click(function() {
            // Recargo la página
            location.reload();
        });
         $( "#btn-send" ).click(function() {
            var postData = new FormData($("#frmPdf")[0]);
            var formURL = "upload.php";
            $.ajax({
                url: formURL,
                type: "POST",
                data: postData,
                //cache: false,
                contentType: false,
                processData: false,
                async: false,
                //mientras enviamos el archivo
                beforeSend: function(){
                message = $("<span class='before'>Subiendo la imagen, por favor espere...</span>");
                $("#message").html(message);
                $("#formpdf").css("display", "none");
                $("#secciontable").css("display", "none");
                },
                success: function(data)
                {
                    $("#message").html(data);
                },
                error: function(jqXHR, textStatus, datos)
                {
                     alert('Error ajax');
                }
            });


            $( ".icon-save" ).click(function() {
            var postData = $('#frmSave').serializeArray();
            var formURL = "savepdf.php";
            $.ajax({
                url : formURL,
                type: "POST",
                async: true,
                data : postData,
                //mientras enviamos el archivo
                beforeSend: function(){
                message = $("<span class='before'>Subiendo la imagen, por favor espere...</span>");
                //$("#message").html(message);
                //$("#formpdf").css("display", "none");
                },
                success: function(data)
                {
                  alert(data);
                },
                error: function(jqXHR, textStatus, datos)
                {
                     alert('Error ajax');
                }
            });
     });
     });
});
